import configparser
import os
from sys import platform

from kink import di

from data.usecase import PlanoDataUseCase, ContractDataUseCase, StructureDataUseCase, TipoParticularidadeUseCase, \
    CoveredItemDataUseCase, PropertyCoveredItemUseCase, LogEntradaUseCase, CoveredVehicleDataUseCase, \
    ParticularityDataUseCase, CoveredLivesDataUseCase, PropertyCoveredDataUseCase
from domain.usecase import ExecuteInsertUpdate, ParticularityUseCase
from infra.repositories import PlanoRepository, ContractRepository, PropertyCoveredItemRepository, StructureRepository, \
    CoveredItemRepository, ParticularityTypeRepository, ParticularityRepository, PropertyCoveredRepository, \
    CoveredLivesRepository, CoveredVehicleRepository, LogEntradaRepository
from tools import get_resource_ini


class Container:
    """Class Container to dependency injection"""

    def __init__(self) -> None:
        config = configparser.ConfigParser()
        config.read(get_resource_ini())
        di["lib_dir"] = os.environ[(config["database"]["LIB_DIR"])]
        di["string_connection"] = config["database"]["STRING_CONNECTION"]

        di[PlanoRepository] = lambda di: PlanoRepository()
        di[ContractRepository] = lambda di: ContractRepository()
        di[PropertyCoveredItemRepository] = lambda di: PropertyCoveredItemRepository()
        di[StructureRepository] = lambda di: StructureRepository()
        di[CoveredItemRepository] = lambda di: CoveredItemRepository()
        di[ParticularityTypeRepository] = lambda di: ParticularityTypeRepository()
        di[ParticularityRepository] = lambda di: ParticularityRepository()
        di[CoveredVehicleRepository] = lambda di: CoveredVehicleRepository()
        di[PropertyCoveredRepository] = lambda di: PropertyCoveredRepository()
        di[CoveredLivesRepository] = lambda di: CoveredLivesRepository()
        di[LogEntradaRepository] = lambda di: LogEntradaRepository()

        di[PropertyCoveredDataUseCase] = lambda di: PropertyCoveredDataUseCase()
        di[PlanoDataUseCase] = lambda di: PlanoDataUseCase()
        di[ContractDataUseCase] = lambda di: ContractDataUseCase()
        di[StructureDataUseCase] = lambda di: StructureDataUseCase()
        di[TipoParticularidadeUseCase] = lambda di: TipoParticularidadeUseCase()
        di[CoveredItemDataUseCase] = lambda di: CoveredItemDataUseCase()
        di[PropertyCoveredItemUseCase] = lambda di: PropertyCoveredItemUseCase()
        di[LogEntradaUseCase] = lambda di: LogEntradaUseCase()
        di[CoveredVehicleDataUseCase] = lambda di: CoveredVehicleDataUseCase()
        di[ExecuteInsertUpdate] = lambda di: ExecuteInsertUpdate()
        di[ParticularityUseCase] = lambda di: ParticularityUseCase()
        di[ParticularityDataUseCase] = lambda di: ParticularityDataUseCase()
        di[CoveredLivesDataUseCase] = lambda di: CoveredLivesDataUseCase()
