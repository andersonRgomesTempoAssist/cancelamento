import pandas as pd
from kink import inject

from data.usecase.property_covered_item_data_use_case import PropertyCoveredItemUseCase
from infra.entities import PropertyCoveredItemEntity
from tools import get_description_field, get_description_field_int


@inject
class BuildPropertyCoveredItemUseCase:
    def __init__(self, property_covered_item_use_case: PropertyCoveredItemUseCase):
        self.property_covered_item_use_case = property_covered_item_use_case

    def build(self, data_frame_merge, fields_structure, id_client) -> list:
        entities = []
        field_structure = fields_structure.query('tabelaDestino == "PROPRITEMCOBERTO"')
        for index, row in data_frame_merge.iterrows():
            logradouro_end_propr = None
            if 'LOGRADOUROENDPROPR' in field_structure['campoDestino'].values and not pd.isnull(
                    row['LOGRADOUROENDPROPR']):
                logradouro_end_propr = row['LOGRADOUROENDPROPR']
                logradouro_end_propr = logradouro_end_propr[0:PropertyCoveredItemEntity.logradouroendpropr.type.length]

            nome_proprietario = get_description_field(field_structure, 'NOMEPROPRIETARIO', row)

            entities.append(PropertyCoveredItemEntity(id_propritemcoberto=row['id_itemcoberto'],
                                                      nomeproprietario=nome_proprietario,
                                                      paisendpropr=get_description_field(field_structure,
                                                                                         'PAISENDPROPR', row),
                                                      nomeproprietariosoundex=get_description_field(field_structure,
                                                                                                    'NOMEPROPRIETARIOSOUNDEX',
                                                                                                    row),
                                                      cnpjcpfpropr=get_description_field(field_structure,
                                                                                         'CNPJCPFPROPR', row),
                                                      ufendpropr=get_description_field(field_structure, 'UFENDPROPR',
                                                                                       row),
                                                      cidadeendpropr=get_description_field(field_structure,
                                                                                           'CIDADEENDPROPR', row),
                                                      bairroendpropr=get_description_field(field_structure,
                                                                                           'BAIRROENDPROPR', row),
                                                      cependpropr=get_description_field(field_structure, 'CEPENDPROPR',
                                                                                        row),
                                                      logradouroendpropr=logradouro_end_propr,
                                                      numeroendpropr=get_description_field(field_structure,
                                                                                           'NUMEROENDPROPR', row),
                                                      complementoendpropr=get_description_field(field_structure,
                                                                                                'COMPLEMENTOENDPROPR',
                                                                                                row),
                                                      ddifonepropr=get_description_field(field_structure,
                                                                                         'DDIFONEPROPR', row),
                                                      dddfonepropr=get_description_field(field_structure,
                                                                                         'DDDFONEPROPR', row),
                                                      fonepropr=get_description_field(field_structure, 'FONEPROPR',
                                                                                      row),
                                                      id_clientecorporativo=id_client,
                                                      cnpjcpfproprpesquisa=get_description_field_int(field_structure,
                                                                                                     'CNPJCPFPROPR',
                                                                                                     row)))
        return entities

    def execute_insert(self, entities):
        return self.property_covered_item_use_case.insert(entities)

    def execute_update(self, entities):
        return self.property_covered_item_use_case.update(entities)
