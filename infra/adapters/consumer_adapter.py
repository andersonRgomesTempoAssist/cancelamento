# pylint: disable=C0111,C0103,R0205
import configparser
import functools
import threading

import pika

from tools import LOGGER, get_resource_ini


class ConsumerAdapter(object):

    def ack_message(self, ch, delivery_tag):
        """Note that `ch` must be the same pika channel instance via which
        the message being ACKed was retrieved (AMQP protocol constraint).
        """
        if ch.is_open:
            ch.basic_ack(delivery_tag)
        else:
            # Channel is already closed, so we can't ACK this message;
            # log and/or do something that makes sense for your app in this case.
            pass

    def do_work(self, conn, ch, delivery_tag, body):
        thread_id = threading.get_ident()
        LOGGER.info('Thread id: %s Delivery tag: %s Message body: %s', thread_id,
                    delivery_tag, body)
        # Sleeping to simulate 10 seconds of work
        cb = functools.partial(self.ack_message, ch, delivery_tag)
        conn.add_callback_threadsafe(cb)

    def on_message(self, ch, method_frame, _header_frame, body, args):
        (conn, thrds, callback_consumers) = args
        delivery_tag = method_frame.delivery_tag
        t = threading.Thread(target=callback_consumers, args=(conn, ch, delivery_tag, body))
        t.start()
        thrds.append(t)

    def __init__(self, callback_consumers):
        config = configparser.ConfigParser()
        config.read(get_resource_ini())
        host_ = config["queue"]["HOST"]
        user = config["queue"]["USER"]
        password = config["queue"]["PASSWORD"]
        port_ = int(config["queue"]["PORT"])
        credentials = pika.PlainCredentials(user, password)

        parameters = pika.ConnectionParameters(
            host=host_, port=port_, credentials=credentials, heartbeat=5)
        connection = pika.BlockingConnection(parameters)

        channel = connection.channel()
        # channel.exchange_declare(
        #     exchange="processamento",
        #     exchange_type=ExchangeType.direct,
        #     passive=False,
        #     durable=True,
        #     auto_delete=False)
        # channel.queue_declare(queue="process.update", auto_delete=True)
        # channel.queue_bind(
        #     queue="process.update", exchange="processamento", routing_key="process.update")
        # Note: prefetch is set to 1 here as an example only and to keep the number of threads created
        # to a reasonable amount. In production you will want to test with different prefetch values
        # to find which one provides the best performance and usability for your solution
        # channel.basic_qos(prefetch_count=1)

        threads = []
        on_message_callback = functools.partial(self.on_message, args=(connection, threads, callback_consumers))
        channel.basic_consume('process.cancel', on_message_callback)

        try:
            channel.start_consuming()
        except KeyboardInterrupt:
            channel.stop_consuming()

        # Wait for all to complete
        for thread in threads:
            thread.join()

        connection.close()

