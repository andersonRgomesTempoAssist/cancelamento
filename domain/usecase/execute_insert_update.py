from kink import inject

from data.usecase.item_coberto_data_usecase import CoveredItemDataUseCase


@inject()
class ExecuteInsertUpdate:

    def __init__(self, covered_item_data_use_case: CoveredItemDataUseCase):
        self.covered_item_data_use_case = covered_item_data_use_case

    def execute_insert(self, entities):
        return self.covered_item_data_use_case.insert_update_covered_item(entities)

    def execute_update(self, entities):
        return self.covered_item_data_use_case.update_covered_item(entities)


