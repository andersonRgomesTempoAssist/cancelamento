from kink import inject

from infra.repositories import CoveredVehicleRepository


@inject()
class CoveredVehicleDataUseCase:
    def __init__(self, repository: CoveredVehicleRepository):
        self.repository = repository

    def insert(self, entities):
        if len(entities) == 0:
            raise
        return self.repository.insert(entities)

    def update(self, entities):
        if len(entities) == 0:
            raise
        return self.repository.update(entities)
