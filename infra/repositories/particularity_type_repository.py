from collections import namedtuple

from infra.config import DBConnectionHandler
from infra.entities import ParticularityTypeEntity
from infra.helpers import ExceptionRepository


class ParticularityTypeRepository:
    """Class Particularity Repository"""

    def find_by_id(self, id_plano):
        for i in DBConnectionHandler.__session__():
            try:
                return i.query(ParticularityEntity).filter(PlanoEntity.id == id_plano).one()
            except sqlalchemy.exc.OperationalError as error:
                raise ExceptionRepository('000', 'Internal server Error')
                i.throw(error)
            except NoResultFound:
                raise ExceptionRepository('000', 'Not found Plans')
            except Exception as error:
                raise ExceptionRepository('000', 'Internal server Error')
                i.throw(error)

    def find_all(self):
        TipoParticularidade = namedtuple(
            "TipoParticularidade",
            [
                "id_tipo_particularidade",
                "descricao",
                "tipo_valor_partic",
            ])
        for i in DBConnectionHandler.__session__():
            try:
                records = i.query(ParticularityTypeEntity).all()
                return [
                    TipoParticularidade(id_tipo_particularidade=record.id_tipo_particularidade,
                                        descricao=record.descricao,
                                        tipo_valor_partic=record.tipo_valor_partic) for record in records]

            except sqlalchemy.exc.OperationalError as error:
                raise ExceptionRepository('000', 'Internal server Error')
                i.throw(error)
            except NoResultFound:
                raise ExceptionRepository('000', 'Not found Plans')
            except Exception as error:
                raise ExceptionRepository('000', 'Internal server Error')
                i.throw(error)
