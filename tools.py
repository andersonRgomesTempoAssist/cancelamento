import configparser
import json
import logging
import logging.config
import sys
import os

import numpy
import pandas as pd
import pika


def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.dirname(__file__), relative_path)


logging.config.fileConfig(fname=resource_path("logger_config.conf"), disable_existing_loggers=False,
                          defaults={"logfilename": "cancelamento.log"})

LOGGER = logging.getLogger(__name__)


def get_resource_ini() -> str:
    LOGGER.debug("RESOURCE PATH %s".format(resource_path("resource.ini")))
    return resource_path("resource.ini")


def get_logger_config() -> str:
    LOGGER.debug("LOGGER PATH %s".format(resource_path("logger_config.conf")))
    return resource_path("logger_config.conf")


def get_description_field(field_structure, field, row):
    filed = field_structure[field_structure['campoDestino'] == field]
    if filed is not None and len(filed) > 0:
        field_description = field_structure[field_structure['campoDestino'] == field]['descricaoCampo'].values[0]
        if pd.isnull(row[field_description]):
            return None
        return row[field_description] if field_description is not None else None
    return None


def get_description_field_date(field_structure, field, row, date_format):
    filed = field_structure[field_structure['campoDestino'] == field]
    if filed is not None and len(filed) > 0:
        field_description = field_structure[field_structure['campoDestino'] == field]['descricaoCampo'].values[0]
        if pd.isnull(row[field_description]):
            return None
        return pd.to_datetime(row[field_description].replace('/', ''),
                              format=date_format).date() if field_description is not None else None
    return None


def get_description_field_int(field_structure, field, row):
    filed = field_structure[field_structure['campoDestino'] == field]

    if filed is not None and len(filed) > 0:
        field_description = field_structure[field_structure['campoDestino'] == field]['descricaoCampo'].values[0]
        if pd.isnull(row[field_description]):
            return None
        return int(row[field_description]) if field_description is not None else None
    return None


def check_type_num(num):
    if num is None:
        return None
    return num.item() if type(num) is numpy.int64 else num


def check_type_dt(data):
    return data.dt.date.values[0] if type(data.values[0]) is numpy.datetime64 else data.values[0].date()


def send_queue_protocol(payload):
    try:
        LOGGER.debug("send to protocol queue \n" + json.dumps(payload))
        config = configparser.ConfigParser()
        config.read(get_resource_ini())
        host_ = config["queue"]["HOST"]
        user = config["queue"]["USER"]
        password = config["queue"]["PASSWORD"]
        port_ = int(config["queue"]["PORT"])
        credentials = pika.PlainCredentials(user, password)

        parameters = pika.ConnectionParameters(
            host=host_, port=port_, credentials=credentials, heartbeat=5)
        connection = pika.BlockingConnection(parameters)

        channel = connection.channel()
        channel.basic_publish(
            exchange="processamento",
            routing_key="process.protocol",
            body=json.dumps(payload),
        )
    except Exception as error:
        LOGGER.error(error)
        raise error
