from sqlalchemy import Column, String, Integer, Date

from infra.config import Base


class CoveredLifeEntity(Base):
    __tablename__ = "VIDACOBERTA"

    id_vidacoberta = Column("ID_VIDACOBERTA", Integer, primary_key=True, nullable=False)
    sexo = Column("SEXO", String)
    nome = Column("NOME", String)
    nomesoundex = Column("NOMESOUNDEX", String)
    profissao = Column("PROFISSAO", String)
    cpf = Column("CPF", String)
    datanascimento = Column("DATANASCIMENTO", Date)
    rg = Column("RG", String)
    estadocivil = Column("ESTADOCIVIL", String)
    id_clientecorporativo = Column("ID_CLIENTECORPORATIVO", Integer)
    cpfpesquisa = Column("CPFPESQUISA", Integer)
