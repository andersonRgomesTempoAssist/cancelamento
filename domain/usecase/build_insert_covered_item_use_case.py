from datetime import datetime

from infra.entities import CoveredItemEntity
from tools import get_description_field_int, get_description_field, get_description_field_date


class BuildInsertCoveredItemUseCase:
    def __init__(self, data_frame_register, id_structure, id_client, date_contract, date_format: str,
                 structure_data_frame):
        self.data_frame_register = data_frame_register
        self.id_structure = id_structure
        self.id_client = id_client
        self.date_contract = date_contract
        self.date_format = date_format
        self.structure_data_frame = structure_data_frame

    def build(self):
        entities = []
        field_structure = self.structure_data_frame.query('tabelaDestino == "ITEMCOBERTO"')
        for index, row in self.data_frame_register[self.data_frame_register['JOB_TO_EXECUTE'] == 'I'].iterrows():
            data_lote = get_description_field_date(field_structure, 'DATA_LOTE', row,  self.date_format)
            codigosusep = get_description_field_int(field_structure, 'CODIGOSUSEP', row)
            id_sucursal = get_description_field_int(field_structure, 'ID_SUCURSAL', row)

            entity = CoveredItemEntity(id_plano=int(row.PLANO),
                                       id_statusitemcoberto=int(row.OPERACAO),
                                       apolice=row.APOLICE,
                                       datainiciovigencia=row.DATAINICIOVIGENCIA,
                                       apoliceitem=row.APOLICEITEM,
                                       datafimvigencia=row.DATAFIMVIGENCIA,
                                       numerocartao=row.NUMEROCARTAO,
                                       id_estrutura=self.id_structure,
                                       matricula=get_description_field(field_structure, 'MATRICULA', row),
                                       lote=get_description_field_int(field_structure, 'LOTE', row),
                                       blanket=get_description_field(field_structure, 'BLANKET', row),
                                       dataprocessamento=datetime.today(),
                                       id_clientecorporativo=self.id_client,
                                       datacadastro=datetime.today().date(),
                                       datainiciocobranca=self.date_contract,
                                       datafimcobranca=row.DATAFIMVIGENCIA,
                                       data_lote=data_lote,
                                       codigosusep=codigosusep,
                                       id_sucursal=id_sucursal)
            entity.id_seq.dispatch.after_create()
            entities.append(entity)

        return entities
