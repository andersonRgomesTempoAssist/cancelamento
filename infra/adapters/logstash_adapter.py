import logging

import logstash


class LogStashHandler:
    """Logstash connection"""

    def __init__(self, host, port, version, logger_level):
        self.host = host
        self.logger = logging.getLogger("pre-processamento-logger")

        if logger_level == "INFO":
            self.logger.setLevel(logging.INFO)
        elif logger_level == "ERROR":
            self.logger.setLevel(logging.ERROR)
        elif logger_level == "WARNING":
            self.logger.setLevel(logging.WARNING)
        elif logger_level == "DEBUG":
            self.logger.setLevel(logging.DEBUG)
        elif logger_level == "CRITICAL":
            self.logger.setLevel(logging.CRITICAL)

        self.logger.addHandler(
            logstash.LogstashHandler(self.host, port, version=version)
        )

    def __logger__(self):
        return self.logger