from kink import inject

from infra.repositories import CoveredLivesRepository


@inject
class CoveredLivesDataUseCase:
    def __init__(self, repository: CoveredLivesRepository):
        self.repository = repository

    def insert(self, entities):
        if len(entities) == 0:
            raise
        return self.repository.insert(entities)

    def update(self, entities):
        if len(entities) == 0:
            raise
        return self.repository.update(entities)
