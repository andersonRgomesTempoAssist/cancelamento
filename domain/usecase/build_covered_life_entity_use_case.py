from kink import inject

from data.usecase import CoveredLivesDataUseCase
from infra.entities import CoveredLifeEntity
from tools import get_description_field, get_description_field_date


@inject()
class BuildCoveredLifeUseCase:
    def __init__(self, date_format: str, covered_lives_data_use_case: CoveredLivesDataUseCase):
        self.covered_lives_data_use_case = covered_lives_data_use_case
        self.date_format = date_format

    def build(self, data_frame_merge, fields_structure, id_client) -> list:
        entities = []
        field_structure = fields_structure.query('tabelaDestino == "VIDACOBERTA"')

        for index, row in data_frame_merge.iterrows():
            cpf_pesquisa = get_description_field(field_structure, 'CPFPESQUISA', row)
            entities.append(
                CoveredLifeEntity(
                    id_vidacoberta=row['id_itemcoberto'],
                    sexo=get_description_field(field_structure, 'SEXO', row),
                    nome=get_description_field(field_structure, 'NOME', row),
                    nomesoundex=get_description_field(field_structure, 'NOMESOUNDEX', row),
                    profissao= get_description_field(field_structure, 'PROFISSAO', row),
                    cpf=get_description_field(field_structure, 'CPF', row),
                    rg=get_description_field(field_structure, 'RG', row),
                    datanascimento=get_description_field_date(field_structure, 'DATANASCIMENTO', row, self.date_format),
                    estadocivil=get_description_field(field_structure, 'ESTADOCIVIL', row),
                    cpfpesquisa=None if cpf_pesquisa is None else int(cpf_pesquisa),
                    id_clientecorporativo=id_client)
            )
        return entities

    def execute_insert(self, entities):
        return self.covered_lives_data_use_case.insert(entities)

    def execute_update(self, entities):
        return self.covered_lives_data_use_case.update(entities)
