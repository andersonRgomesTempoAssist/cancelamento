import pandas as pd

from domain.usecase.validation import IValidation


class NecessaryField(IValidation):

    def __init__(self, data_frame_register, data_frame_structure):
        self.data_frame_register = data_frame_register
        self.data_frame_structure = data_frame_structure

    def build_validation(self):
        error_message = '[001] Os seguintes campos não possuem valor '
        if 'MESSAGE_ERROR' not in self.data_frame_register.columns:
            self.data_frame_register['MESSAGE_ERROR'] = None

        if self.data_frame_register['MESSAGE_ERROR'].dtypes == 'float64':
            self.data_frame_register["MESSAGE_ERROR"] = self.data_frame_register["MESSAGE_ERROR"].astype(
                {'MESSAGE_ERROR': str})
            self.data_frame_register['MESSAGE_ERROR'] = None

        for column in self.data_frame_register.columns:
            index = self.data_frame_structure[self.data_frame_structure['descricaoCampo'] == column].index.values
            try:
                if self.data_frame_structure['obrigatorio'][index[0]] == 'S':
                    message = (error_message + column)

                    index = self.data_frame_register[pd.isnull(self.data_frame_register['MESSAGE_ERROR'])][
                        self.data_frame_register[column] == ''].index.values
                    if len(index) > 0:
                        for index_ in index:
                            self.data_frame_register.at[index_, 'MESSAGE_ERROR'] = message

                    index = self.data_frame_register[self.data_frame_register['MESSAGE_ERROR'].isnull()][
                        self.data_frame_register[column].isnull()].index.values

                    if len(index) > 0:
                        for index_ in index:
                            self.data_frame_register.at[index_, 'MESSAGE_ERROR'] = message

            except Exception as error:
                print(column, error)

        return self.data_frame_register
