from collections import namedtuple

import sqlalchemy
from sqlalchemy.orm.exc import NoResultFound

from infra.config import DBConnectionHandler
from infra.entities import ContractEntity
from infra.helpers import ExceptionRepository


class ContractRepository:

    def find_by_id(self, id_contract):
        for i in DBConnectionHandler.__session__():
            try:
                result = i.query(ContractEntity).filter(
                    ContractEntity.id_contrato == id_contract).one()
                Contrato = namedtuple(
                    "Contrato",
                    [
                        "id_contrato",
                        "id_status",
                        "id_clientecorporativo",
                        "numero",
                        "titulo",
                        "datainicio",
                        "datavalidade",
                        "datacadastro"
                    ])
                return Contrato(id_contrato=result.id_contrato, id_status=result.id_status,
                                id_clientecorporativo=result.id_clientecorporativo,
                                numero=result.id_clientecorporativo,
                                titulo=result.titulo, datainicio=result.datainicio,
                                datavalidade=result.datavalidade, datacadastro=result.datacadastro)
            except sqlalchemy.exc.OperationalError as error:
                raise ExceptionRepository('000', 'Internal server Error')
                i.throw(error)
            except NoResultFound:
                raise ExceptionRepository('000', 'Not found Plans')
            except Exception as error:
                raise ExceptionRepository('000', 'Internal server Error')
                i.throw(error)
