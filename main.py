import datetime
import functools
import json
import logging
import os
import threading

import time
from collections import Counter

import pandas as pd
from kink import di
from pyreadstat import pyreadstat

from container import Container
from date_format_enum import DateFormatEnum
from domain.usecase import ReadFileUseCase, LoadInformationUseCase, SaveLogInputUseCase, BuildUpdateUseCase, \
    BuildInsertCoveredItemUseCase, ExecuteInsertUpdate, BuildPropertyCoveredItemUseCase, ParticularityUseCase, \
    BuildCoveredVehicleUseCase, PropertyCoveredUseCase, BuildCoveredLifeUseCase
from domain.usecase.validation.impl import NecessaryField, PlanValidation, DateValidate
from infra.adapters import ConsumerAdapter
from tools import send_queue_protocol

LOGGER = logging.getLogger(__name__)


def callback_consumers(body):
    LOGGER.info('Registration processing started at %s ', datetime.date.today)
    str_folder = body['file']
    LOGGER.debug("capture the main path %s ", str_folder)
    path_register_sav = os.path.join(str_folder, 'cancel.sav')
    LOGGER.debug("capture the path *.sav file %s", path_register_sav)
    os.chdir(str_folder)
    os.chdir('../')
    path_structure_sav = os.path.join(os.getcwd(), 'structure.sav')
    contract_id = body['contract_id']
    structure_id = body['structure_id']
    client_id = body['client']
    id_log = body['id_log']
    log_input = di[SaveLogInputUseCase]
    input_log_model = log_input.find_by_id(id_log)
    LOGGER.debug("Log file founded, id_log %s", id_log)
    di["id_log"] = id_log
    di["path"] = os.getcwd()
    step_load_information(path_register_sav, path_structure_sav, contract_id, structure_id, client_id, input_log_model)


def step_load_information(path_register_sav, path_structure_sav, contract_id, structure, client_id, input_log_model):
    load_info = di[LoadInformationUseCase]
    plans_by_contract = load_info.load_information_plan_by_contract(contract_id)
    contract_by_id = load_info.load_contract_by_id(contract_id)
    structure_by_id = load_info.load_structure_by_id(structure)
    type_particularity = load_info.load_type_particularity()

    step_find_files(path_register_sav=path_register_sav, path_structure_sav=path_structure_sav,
                    contract_by_id=contract_by_id, plans_by_contract=plans_by_contract,
                    structure_by_id=structure_by_id, client_id=client_id, type_particularity=type_particularity,
                    input_log_model=input_log_model)


def step_find_files(path_register_sav, path_structure_sav, plans_by_contract, contract_by_id, structure_by_id,
                    client_id, type_particularity, input_log_model):
    read_file_use_case = ReadFileUseCase(path_register_sav)
    df_registers = read_file_use_case.reader_file()
    read_file_use_case.set_path(path_structure_sav)
    df_structure = read_file_use_case.reader_file()
    step_validation_register(contract_by_id=contract_by_id, structure_data_frame=df_structure,
                             register_data_frame=df_registers,
                             plans_by_contract=plans_by_contract,
                             structure=structure_by_id, client_id=client_id,
                             type_particularity=type_particularity, input_log_model=input_log_model)


def step_validation_register(contract_by_id, structure_data_frame, register_data_frame, plans_by_contract, structure,
                             client_id, type_particularity, input_log_model):
    format_str = DateFormatEnum.__members__[structure.formatodata.replace("/", "")].value
    register_data_frame = NecessaryField(data_frame_register=register_data_frame,
                                         data_frame_structure=structure_data_frame).build_validation()
    register_data_frame = PlanValidation(data_frame=register_data_frame, plans=plans_by_contract).build_validation()
    register_data_frame = DateValidate(data_frame_register=register_data_frame,
                                       contract=contract_by_id, format_str=format_str).build_validation()
    step_validation_structure(structure=structure, contract_by_id=contract_by_id,
                              register_data_frame=register_data_frame, client_id=client_id,
                              structure_data_frame=structure_data_frame, type_particularity=type_particularity,
                              date_format=format_str, input_log_model=input_log_model)


def step_validation_structure(structure, contract_by_id, client_id, register_data_frame, structure_data_frame,
                              type_particularity, date_format, input_log_model):
    if structure.inclusao_automatica == 'S':
        load_info = di[LoadInformationUseCase]
        array_policy = register_data_frame['APOLICE'][register_data_frame['MESSAGE_ERROR'].isnull()].values
        array_policy_item = register_data_frame['APOLICEITEM'][register_data_frame['MESSAGE_ERROR'].isnull()].values
        array_car_number = register_data_frame['NUMEROCARTAO'][register_data_frame['MESSAGE_ERROR'].isnull()].values
        array_id_plano = register_data_frame['PLANO'][register_data_frame['MESSAGE_ERROR'].isnull()].values
        array_policy = [k for k, v in Counter(array_policy).items()]
        array_policy_item = [k for k, v in Counter(array_policy_item).items()]
        array_car_number = [k for k, v in Counter(array_car_number).items()]
        array_id_plano = [k for k, v in Counter(array_id_plano).items()]
        covered_item = load_info.load_covered_item(client_id, contract_by_id.id_contrato, array_policy,
                                                   array_policy_item,
                                                   array_car_number, array_id_plano)
        data_frame_covered_item = pd.DataFrame(data=covered_item)
        if 'JOB_TO_EXECUTE' not in register_data_frame.columns:
            register_data_frame['JOB_TO_EXECUTE'] = None
        if data_frame_covered_item.size > 0:
            index = register_data_frame.query('MESSAGE_ERROR.isnull() and APOLICE in ("' + '","'.join(
                data_frame_covered_item['apolice'].unique()) + '") and APOLICEITEM in ("' + '","'.join(
                data_frame_covered_item['apoliceitem'].unique()) + '") and NUMEROCARTAO in ("' + '","'.join(
                data_frame_covered_item['numerocartao'].unique()) + '")').index.values

            register_data_frame.at[index, 'JOB_TO_EXECUTE'] = 'A'

        index = register_data_frame.query("MESSAGE_ERROR.isnull() and JOB_TO_EXECUTE.isnull()").index.values
        register_data_frame.at[index, 'JOB_TO_EXECUTE'] = 'I'

        step_build_insert_update(register_data_frame, client_id, structure.id_estrutura, contract_by_id.datavalidade,
                                 data_frame_covered_item, structure_data_frame,
                                 type_particularity, date_format,
                                 input_log_model)


def step_build_insert_update(register_data_frame, id_client, id_structure, date_contract,
                             data_frame_covered_item,
                             structure_data_frame, type_particularity, date_format, input_log_model):
    update_built = []
    if len(data_frame_covered_item) > 0:
        build_update = BuildUpdateUseCase(data_frame_covered_item=data_frame_covered_item, id_client=id_client,
                                          data_frame_register=register_data_frame, id_structure=id_structure,
                                          date_contract=date_contract, date_format=date_format,
                                          structure_data_frame=structure_data_frame)
        update_built = build_update.build_update()

    build_insert = BuildInsertCoveredItemUseCase(data_frame_register=register_data_frame, id_client=id_client,
                                                 id_structure=id_structure,
                                                 date_contract=date_contract, date_format=date_format,
                                                 structure_data_frame=structure_data_frame)
    insert_built = build_insert.build()

    step_execute_register(register_data_frame, insert_built, update_built, structure_data_frame, id_client,
                          type_particularity, date_format, input_log_model=input_log_model)


def step_execute_register(register_data_frame, insert_built, update_built, structure_data_frame, id_client,
                          type_particularity, date_format, input_log_model):
    execute_insert_update = di[ExecuteInsertUpdate]
    LOGGER.info("you have %s records to update ", len(update_built))
    if len(update_built) > 0:
        record_update = execute_insert_update.execute_update(update_built)
        build_complement_information(register_data_frame, structure_data_frame, record_update, id_client, 'UPDATE',
                                     type_particularity, date_format)
    LOGGER.info("you have %s records to include ", len(insert_built))
    if len(insert_built) > 0:
        record_insert = execute_insert_update.execute_insert(insert_built)

        build_complement_information(register_data_frame, structure_data_frame, record_insert, id_client, 'INSERT',
                                     type_particularity, date_format)

    path = di["path"]
    if register_data_frame['MESSAGE_ERROR'].dtypes == 'float64':
        index = register_data_frame.query("MESSAGE_ERROR.isna() == True").index.values
        register_data_frame["MESSAGE_ERROR"] = register_data_frame["MESSAGE_ERROR"].astype(
            {'MESSAGE_ERROR': str})
        if len(index) > 0:
            for index_ in index:
                register_data_frame.at[index_, 'MESSAGE_ERROR'] = None

    pyreadstat.write_sav(register_data_frame, path + "/logger/cancel.sav")

    payload = {
        "file": path + "/logger/cancel.sav",
        "id_log": di["id_log"]
    }
    send_queue_protocol(payload)
    LOGGER.info('Registration processing finished at %s ', datetime.date.today)


def build_complement_information(register_data_frame, structure_data_frame, record_insert_update, id_client, is_update,
                                 type_particularity, date_format):
    threads_return = []
    threads = []

    build_property_covered_item = BuildPropertyCoveredItemUseCase()
    builder_particularity = ParticularityUseCase()
    builder_covered_vehicle = BuildCoveredVehicleUseCase()
    builder_property_covered = PropertyCoveredUseCase()
    build_covered_life_covered = BuildCoveredLifeUseCase(date_format=date_format)
    data_frame_covered_items = pd.DataFrame([s.__dict__ for s in record_insert_update])
    job = 'A' if is_update == 'UPDATE' else 'I'
    data_frame_covered_items['apolice'] = data_frame_covered_items['apolice'].astype(str)
    data_frame_merge = pd.merge(left=data_frame_covered_items,
                                right=register_data_frame.query(
                                    'MESSAGE_ERROR.isnull() and JOB_TO_EXECUTE == "' + job + '"'),
                                left_on=['apolice', 'apoliceitem', 'numerocartao'],
                                right_on=['APOLICE', 'APOLICEITEM', 'NUMEROCARTAO'], how='inner')

    if 'PROPRITEMCOBERTO' in structure_data_frame['tabelaDestino'].values:
        t1 = threading.Thread(name="property_covered_items", target=thread_property_covered_items,
                              args=(build_property_covered_item, data_frame_merge, structure_data_frame, id_client))
        threads.append(t1)

    if 'VEICULOCOBERTO' in structure_data_frame['tabelaDestino'].values:
        t2 = threading.Thread(name="covered_vehicles", target=thread_covered_vehicles,
                              args=(builder_covered_vehicle, data_frame_merge, structure_data_frame, id_client))
        threads.append(t2)

    if 'VIDACOBERTA' in structure_data_frame['tabelaDestino'].values:
        t3 = threading.Thread(name="covered_lives", target=thread_covered_life,
                              args=(build_covered_life_covered, data_frame_merge,
                                    structure_data_frame,
                                    id_client))
        threads.append(t3)

    if 'IMOVELCOBERTO' in structure_data_frame['tabelaDestino'].values:
        t4 = threading.Thread(name="Property_covered", target=thread_property_covered,
                              args=(builder_property_covered, data_frame_merge,
                                    structure_data_frame,
                                    id_client))
        threads.append(t4)

    if 'PARTICULARIDADE' in structure_data_frame['tabelaDestino'].values:
        t5 = threading.Thread(name="particularities", target=thread_particularities,
                              args=(builder_particularity, record_insert_update, data_frame_merge, structure_data_frame,
                                    type_particularity))
        threads.append(t5)

    for thread_ in threads:
        threads_return.append(thread_.start())

    check_finished = len(threads)

    while check_finished > 0:
        for thread_ in threads:
            if not thread_.is_alive():
                LOGGER.info("Finished Thread, %s", thread_.name)
                check_finished -= 1
                threads.remove(thread_)
        time.sleep(5)

    LOGGER.info('FINISHED PROCESS')


def thread_property_covered_items(build_property_covered_item, data_frame_merge, structure_data_frame, id_client):
    LOGGER.info("Start Thread property_covered_items")
    property_covered_items = build_property_covered_item.build(data_frame_merge,
                                                               structure_data_frame,
                                                               id_client)
    if property_covered_items is not None and len(property_covered_items) > 0:
        return build_property_covered_item.execute_update(property_covered_items)
    LOGGER.info("FINISHED property_covered_items")


def thread_covered_vehicles(builder_covered_vehicle, data_frame_merge, structure_data_frame, id_client):
    LOGGER.info("Start Thread covered_vehicles")
    covered_vehicles = builder_covered_vehicle.build(data_frame_merge,
                                                     structure_data_frame,
                                                     id_client)

    if covered_vehicles is not None and len(covered_vehicles) > 0:
        return builder_covered_vehicle.execute_update(covered_vehicles)
    LOGGER.info("FINISHED property_covered_items")


def thread_covered_life(build_covered_life_covered, data_frame_merge,
                        structure_data_frame,
                        id_client):
    LOGGER.info("Start Thread covered_lives")
    covered_lives = build_covered_life_covered.build(data_frame_merge,
                                                     structure_data_frame,
                                                     id_client)

    if covered_lives is not None and len(covered_lives) > 0:
        return build_covered_life_covered.execute_update(covered_lives)
    LOGGER.info("FINISHED property_covered_items")


def thread_property_covered(builder_property_covered, data_frame_merge,
                            structure_data_frame,
                            id_client):
    LOGGER.info("Start Thread thread_property_covered")
    property_covered = builder_property_covered.build(data_frame_merge,
                                                      structure_data_frame,
                                                      id_client)

    if property_covered is not None and len(property_covered) > 0:
        return builder_property_covered.execute_update(property_covered)
    LOGGER.info("FINISHED thread_property_covered")

    return builder_property_covered.execute_update(property_covered)


def thread_particularities(builder_particularity, record_insert_update, data_frame_merge, structure_data_frame,
                           type_particularity):
    LOGGER.info("Start Thread thread_particularities")
    list_item_coberto = [covered_item.id_itemcoberto for covered_item in record_insert_update]
    particularities = builder_particularity.build(data_frame_merge, structure_data_frame,
                                                  type_particularity, list_item_coberto)
    if particularities is not None and len(particularities) > 0:
        return builder_particularity.execute_update(particularities)
    LOGGER.info("FINISHED thread_particularities")


def do_work(conn, ch, delivery_tag, body):
    thread_id = threading.get_ident()
    LOGGER.info('Thread id: %s Delivery tag: %s Message body: %s', thread_id,
                delivery_tag, body)
    # Sleeping to simulate 10 seconds of work
    cb = functools.partial(ack_message, ch, delivery_tag)
    conn.add_callback_threadsafe(cb)
    obj = json.loads(body.decode('UTF-8'))
    callback_consumers(obj)


def ack_message(ch, delivery_tag):
    """Note that `ch` must be the same pika channel instance via which
    the message being ACKed was retrieved (AMQP protocol constraint).
    """
    if ch.is_open:
        ch.basic_ack(delivery_tag)
    else:
        # Channel is already closed, so we can't ACK this message;
        # log and/or do something that makes sense for your app in this case.
        pass


if __name__ == '__main__':
    Container()
    ConsumerAdapter(do_work)
