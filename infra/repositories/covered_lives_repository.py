from collections import namedtuple

import sqlalchemy

from infra.config import DBConnectionHandler
from infra.entities import CoveredLifeEntity
from infra.helpers import ExceptionRepository


class CoveredLivesRepository:

    def insert(self, entities):
        for i in DBConnectionHandler.__session__():
            try:
                i.bulk_save_objects(entities, return_defaults=True)
                i.commit()
                return entities
            except sqlalchemy.exc.OperationalError as error:
                raise ExceptionRepository('000', 'Internal server Error')
                i.throw(error)
            except Exception as error:
                i.throw(error)
                raise ExceptionRepository('000', 'Internal server Error')

    def find_list_id_covered_item(self, covered_item: []):
        Record = namedtuple("Record", [
            "id_vidacoberta",
            "sexo",
            "nome",
            "nomesoundex",
            "profissao",
            "cpf",
            "datanascimento",
            "rg",
            "estadocivil",
            "id_clientecorporativo",
            "cpfpesquisa"
        ])
        for i in DBConnectionHandler.__session__():
            try:
                if len(covered_item) > 1000:
                    filter_select = ""
                    str_query = "select ID_VIDACOBERTA, " \
                                "SEXO,NOME,NOMESOUNDEX, " \
                                "PROFISSAO,CPF,DATANASCIMENTO,RG,ESTADOCIVIL," \
                                "ID_CLIENTECORPORATIVO CPFPESQUISA FROM VIDACOBERTA where 1=1"

                    n = len(covered_item) // 1000 + 1
                    splinted = [covered_item[i::n] for i in range(n)]
                    for sp in splinted:
                        filter_select += " OR ID_VIDACOBERTA IN (" + str(sp).replace('[', '').replace(']',
                                                                                                      '') + ")"
                    str_query += " AND (" + filter_select[3:] + ")"
                    results = i.execute(str_query)
                    Record = namedtuple("Record", results.keys())
                    records = [Record(*r) for r in results.fetchall()]
                    return records
                else:
                    results = i.query(CoveredLifeEntity).filter(
                        CoveredLifeEntity.id_vidacoberta.in_(covered_item)).all()
                    return [
                        Record(
                            id_vidacoberta=result.id_vidacoberta,
                            sexo=result.sexo,
                            nome=result.nome,
                            nomesoundex=result.nomesoundex,
                            profissao=result.profissao,
                            cpf=result.cpf,
                            datanascimento=result.datanascimento,
                            rg=result.rg,
                            estadocivil=result.estadocivil,
                            id_clientecorporativo=result.id_clientecorporativo,
                            cpfpesquisa=result.cpfpesquisa
                        )
                        for result in results
                    ]
            except sqlalchemy.exc.OperationalError as error:
                raise ExceptionRepository('000', 'Internal server Error')
                i.throw(error)
            except Exception as error:
                i.throw(error)
                raise ExceptionRepository('000', 'Internal server Error')

    def update(self, entities):
        mappings = []
        mappings_insert = []
        id_vidacoberta = [entity.id_vidacoberta for entity in entities]
        list_properties = self.find_list_id_covered_item(id_vidacoberta)
        if len(list_properties) > 0:
            for entity in entities:
                exists = list(filter(lambda x: entity.id_vidacoberta in x, list_properties))
                if entity.id_vidacoberta is not None and len(exists) > 0:
                    mapper = {'id_vidacoberta': entity.id_vidacoberta,
                              'sexo': entity.sexo,
                              'nome': entity.nome,
                              'nomesoundex': entity.nomesoundex,
                              'profissao': entity.profissao,
                              'cpf': entity.cpf,
                              'datanascimento': entity.datanascimento,
                              'rg': entity.rg,
                              'estadocivil': entity.estadocivil,
                              'id_clientecorporativo': entity.id_clientecorporativo,
                              'cpfpesquisa': entity.cpfpesquisa
                              }
                    mappings.append(mapper)
                else:
                    mappings_insert.append(entity)
        else:
            return self.insert(entities)
        if len(mappings_insert) > 0:
            self.insert(mappings_insert)
        if len(mappings) > 0:
            for i in DBConnectionHandler.__session__():
                try:
                    i.bulk_update_mappings(CoveredLifeEntity, mappings)
                    i.commit()
                    return entities
                except sqlalchemy.exc.OperationalError as error:
                    raise ExceptionRepository('000', 'Internal server Error')
                    i.throw(error)
                except Exception as error:
                    i.throw(error)
                    raise ExceptionRepository('000', 'Internal server Error')
