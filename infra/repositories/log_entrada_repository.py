import sqlalchemy
from sqlalchemy.orm.exc import NoResultFound

from data.models import InputLogModel
from infra.config import DBConnectionHandler
from infra.entities import LogEntradaEntity


class LogEntradaRepository:
    """Class LogEntrada Repository"""

    def find_by_id(self, id_seqlog):
        for i in DBConnectionHandler.__session__():
            try:
                record = i.query(LogEntradaEntity).get(id_seqlog)
                return InputLogModel(
                    id_seqlog=record.id_seqlog,
                    id_clientecorporativo=record.id_clientecorporativo,
                    id_contrato=record.id_contrato,
                    data=record.data,
                    id_tipocarteira=record.id_tipocarteira,
                    hora=record.hora,
                    arquivoorigem=record.arquivoorigem,
                    totalregistros=record.totalregistros,
                    regcorretos=record.regcorretos,
                    regincorretos=record.regincorretos,
                    regenvinclusao=record.regenvinclusao,
                    regenvalteracao=record.regenvalteracao,
                    regenvcancelamento=record.regenvcancelamento,
                    regenvreativacao=record.regenvreativacao,
                    regenvprecadastro=record.regenvprecadastro,
                    regenverro=record.regenverro,
                    regenvfatal1=record.regenvfatal1,
                    regenvfatal2=record.regenvfatal2,
                    regenvvencido=record.regenvvencido,
                    regenvlote=record.regenvlote,
                    data_fim=record.data_fim,
                    hora_fim=record.hora_fim,
                    status=record.status,
                    id_estrutura=record.id_estrutura,
                    regjacadastrado=record.regjacadastrado,
                    regnaoprocessado=record.regnaoprocessado,
                    regignorado=record.regignorado,
                    regduplicadocarga=record.regduplicadocarga)
            except sqlalchemy.exc.OperationalError as error:
                i.throw(error)
            except NoResultFound as error:
                return None
            except Exception as error:
                raise error

    def find_register(self, id_corporate_client, id_contract, id_wallet_type, id_struct, name_file_origin):
        for i in DBConnectionHandler.__session__():
            try:
                entity = i.query(LogEntradaEntity).filter(
                    (LogEntradaEntity.id_clientecorporativo == id_corporate_client) &
                    (LogEntradaEntity.id_contrato == id_contract) &
                    (LogEntradaEntity.id_tipocarteira == id_wallet_type) &
                    (LogEntradaEntity.id_estrutura == id_struct) &
                    (LogEntradaEntity.arquivoorigem.like("%" + name_file_origin + "%"))).one()

                return entity.id_seqlog
            except sqlalchemy.exc.OperationalError as error:
                i.throw(error)
            except NoResultFound as error:
                return None
            except Exception as error:
                i.throw(error)

    def update(self, entity: LogEntradaEntity):
        for i in DBConnectionHandler.__session__():
            try:
                i.merge(entity)
                i.commit()
                return entity.id_seqlog
            except sqlalchemy.exc.OperationalError as error:
                i.throw(error)
            except Exception as error:
                i.throw(error)

    def insert(self, entity: LogEntradaEntity):
        if entity.id_seqlog is not None:
            return self.update(entity)
        else:
            for i in DBConnectionHandler.__session__():
                try:
                    i.add(entity)
                    i.commit()
                    return entity.id_seqlog
                except sqlalchemy.exc.OperationalError as error:
                    i.throw(error)
                except Exception as error:
                    i.throw(error)
