from datetime import datetime

import pandas as pd

from infra.entities import CoveredItemEntity
from tools import check_type_dt, get_description_field_int, get_description_field_date, get_description_field


class BuildUpdateUseCase:
    def __init__(self, data_frame_register, id_structure, id_client, date_contract, data_frame_covered_item,
                 date_format: str, structure_data_frame):
        self.data_frame_register = data_frame_register
        self.id_structure = id_structure
        self.id_client = id_client
        self.date_contract = date_contract
        self.data_frame_covered_item = data_frame_covered_item
        self.date_format = date_format
        self.structure_data_frame = structure_data_frame
        self.field_structure = self.structure_data_frame.query('tabelaDestino == "ITEMCOBERTO"')

    def build_update(self):
        entities = []
        data_frame = self.data_frame_register[pd.isnull(self.data_frame_register['MESSAGE_ERROR'])]
        self._process(data_frame, entities)
        return entities

    def _process(self, data_frame, entities):
        item_coberto_data_base = pd.merge(left=self.data_frame_covered_item,
                                          right=data_frame.query(
                                              'JOB_TO_EXECUTE == "A"'),
                                          left_on=['apolice', 'apoliceitem', 'numerocartao'],
                                          right_on=['APOLICE', 'APOLICEITEM', 'NUMEROCARTAO'])

        for index, row in item_coberto_data_base.iterrows():
            # Valida se o plano está passível de alteração
            if len(item_coberto_data_base) > 0:
                if row.PLANO != str(item_coberto_data_base.id_plano.values[0]):
                    date_start_validity = None if pd.isnull(row.DATAINICIOVIGENCIA) else row.DATAINICIOVIGENCIA
                    today = datetime.today().date()
                    if date_start_validity <= today:
                        if date_start_validity >= check_type_dt(item_coberto_data_base.datainiciovigencia):
                            datafimvigencia = row.DATAFIMVIGENCIA
                            datafimcobranca = None
                            # Validar endosso fim de vigencia
                            if datafimvigencia > row.datafimvigencia.date() or (
                                    datafimvigencia < row.datafimvigencia.date() and (
                                    datafimvigencia > today)):
                                datafimcobranca = row.DATAFIMVIGENCIA

                            elif datafimvigencia < row.datafimvigencia.date() and (
                                    datafimvigencia <= today <= row.datafimvigencia.date()
                            ):
                                datafimcobranca = today
                            elif datafimvigencia < row.datafimvigencia.date() < today:
                                datafimcobranca = row.datafimvigencia.date()
                            entities = self._build_update(canceled_old_register=True,
                                                          row=row,
                                                          datafimcobranca=datafimcobranca,
                                                          entities=entities, datafimvigencia=datafimvigencia)
                        else:
                            message = '[007] Data de Inicio informada é menor que a Data de Inicio' \
                                      ' existente no registro na USS'
                            data_frame.at[index, 'MESSAGE_ERROR'] = message
                    else:
                        message = '[016] Data de inicio de vigência enviada é maior que data de processamento'
                        data_frame.at[index, 'MESSAGE_ERROR'] = message
                else:
                    entities = self._build_update(canceled_old_register=False, row=row,
                                                  datafimcobranca=row.datafimcobranca.date(),
                                                  entities=entities, datafimvigencia=row.DATAFIMVIGENCIA)
            else:
                data_frame.at[index, 'JOB_TO_EXECUTE'] = 'I'

    def _build_update(self, canceled_old_register, row,
                      datafimcobranca, entities, datafimvigencia):
        if canceled_old_register:
            entities.append(CoveredItemEntity(id_itemcoberto=int(row.id_itemcoberto),
                                              id_plano=int(row.id_plano),
                                              id_statusitemcoberto=3,
                                              apolice=str(row.apolice),
                                              datainiciovigencia=
                                              row.datainiciovigencia,
                                              apoliceitem=str(row.apoliceitem),
                                              datafimvigencia=row.datafimvigencia,
                                              numerocartao=str(row.numerocartao),
                                              id_estrutura=int(
                                                  row.id_estrutura) if not pd.isnull(row.id_estrutura) else None,
                                              matricula=str(row.matricula),
                                              lote=int(row.lote) if row.lote is not None else None,
                                              blanket=str(row.blanket) if not pd.isnull(row.blanket) else None,
                                              dataprocessamento=row.dataprocessamento,
                                              id_clientecorporativo=int(
                                                  row.id_clientecorporativo) if not pd.isnull(
                                                  row.id_clientecorporativo) else None,
                                              datacadastro=row.datacadastro,
                                              datacancelamento=row.datacancelamento,
                                              datainiciocobranca=row.datainiciocobranca,
                                              datafimcobranca=row.datafimcobranca,
                                              qtddiasendosso=int(
                                                  row.qtddiasendosso) if not pd.isnull(
                                                  row.qtddiasendosso) else None,
                                              id_statusprovisorio=int(
                                                  row.id_statusprovisorio) if not pd.isnull(
                                                  row.id_statusprovisorio) else None,
                                              datastatusprovisorio=row.datastatusprovisorio,
                                              datareativacao=row.datareativacao,
                                              data_lote=row.data_lote,
                                              data_expurgo=row.data_expurgo,
                                              codigosusep=int(row.codigosusep) if not pd.isnull(
                                                  row.codigosusep) else None,
                                              id_sucursal=int(
                                                  row.id_sucursal) if not pd.isnull(row.id_sucursal) else None))

            entities.append(CoveredItemEntity(id_plano=int(row.PLANO),
                                              id_statusitemcoberto=int(row.OPERACAO),
                                              apolice=str(row.APOLICE),
                                              datainiciovigencia=row.DATAINICIOVIGENCIA,
                                              apoliceitem=str(row.APOLICEITEM),
                                              datafimvigencia=datafimvigencia,
                                              numerocartao=str(row.NUMEROCARTAO),
                                              id_estrutura=int(self.id_structure),
                                              matricula=get_description_field(self.field_structure, 'MATRICULA', row),
                                              lote=get_description_field_int(self.field_structure, 'LOTE', row),
                                              blanket=get_description_field(self.field_structure, 'BLANKET', row),
                                              dataprocessamento=datetime.today(),
                                              id_clientecorporativo=int(self.id_client),
                                              datacadastro=datetime.today().date(),
                                              datacancelamento=None,
                                              datainiciocobranca=datetime.today().date(),
                                              datafimcobranca=row.DATAFIMVIGENCIA,
                                              qtddiasendosso=None,
                                              id_statusprovisorio=None,
                                              datastatusprovisorio=None,
                                              datareativacao=None,
                                              data_lote=get_description_field_date(self.field_structure, 'DATA_LOTE',
                                                                                   row,
                                                                                   self.date_format),
                                              data_expurgo=None,
                                              codigosusep=get_description_field_int(self.field_structure, 'CODIGOSUSEP',
                                                                                    row),
                                              id_sucursal=get_description_field_int(self.field_structure, 'ID_SUCURSAL',
                                                                                    row)))
        else:
            entities.append(CoveredItemEntity(id_itemcoberto=int(row.id_itemcoberto),
                                              id_plano=int(row.PLANO),
                                              id_statusitemcoberto=int(row.OPERACAO),
                                              apolice=str(row.APOLICE),
                                              datainiciovigencia=row.DATAINICIOVIGENCIA,
                                              apoliceitem=str(row.APOLICEITEM),
                                              datafimvigencia=datafimvigencia,
                                              numerocartao=str(row.NUMEROCARTAO),
                                              id_estrutura=int(self.id_structure),
                                              matricula=get_description_field(self.field_structure, 'MATRICULA', row),
                                              lote=get_description_field_int(self.field_structure, 'LOTE', row),
                                              blanket=get_description_field(self.field_structure, 'BLANKET', row),
                                              dataprocessamento=datetime.today(),
                                              id_clientecorporativo=int(self.id_client),
                                              datacadastro=datetime.today().date(),
                                              datacancelamento=None,
                                              datainiciocobranca=datetime.today().date(),
                                              datafimcobranca=datafimcobranca,
                                              qtddiasendosso=None,
                                              id_statusprovisorio=None,
                                              datastatusprovisorio=None,
                                              datareativacao=None,
                                              data_lote=get_description_field_date(self.field_structure, 'DATA_LOTE',
                                                                                   row,
                                                                                   self.date_format),
                                              data_expurgo=None,
                                              codigosusep=get_description_field_int(self.field_structure, 'CODIGOSUSEP',
                                                                                    row),
                                              id_sucursal=get_description_field_int(self.field_structure, 'ID_SUCURSAL',
                                                                                    row)))
        return entities
