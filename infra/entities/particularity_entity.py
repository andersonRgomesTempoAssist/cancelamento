from sqlalchemy import Column, String, BigInteger, Integer, Sequence

from infra.config import Base


class ParticularityEntity(Base):
    __tablename__ = "PARTICULARIDADE"

    id_seq = Sequence('ID_PARTICULARIDADE', metadata=Base.metadata)
    id_particularidade = Column("ID_PARTICULARIDADE", BigInteger, id_seq, nullable=False, primary_key=True)
    id_itemcoberto = Column("ID_ITEMCOBERTO", Integer, nullable=False)
    id_tipoparticularidade = Column("ID_TIPOPARTICULARIDADE", Integer, nullable=False)
    valor = Column("VALOR", String(60), nullable=False)

# def __eq__(self, other):
#     return self.id_particularidade == other.id_particularidade \
#            and self.id_itemcoberto == other.id_itemcoberto \
#            and self.id_tipoparticularidade == other.id_tipoparticularidade \
#            and self.valor == other.valor
