from enum import Enum


class DateFormatEnum(Enum):
    YYYYMMDD = "%Y%m%d"
    DDMMYYYY = "%d%m%Y"
    DDMMYY = "%d%m%Y"
    YYYYMMDDD = "Y%m%d"
    DDMMAAAA = "%d%m%Y"
    AAAAMMDD = "%Y%m%d"
