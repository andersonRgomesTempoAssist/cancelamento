from kink import inject

from data.helpers import ExceptionData
from data.models import InputLogModel
from infra.entities import LogEntradaEntity
from infra.repositories import LogEntradaRepository


@inject()
class LogEntradaUseCase:

    def __init__(self, repository: LogEntradaRepository):
        self.repository = repository

    def find_by_id(self, id_seqlog):
        return self.repository.find_by_id(id_seqlog)

    def find_file(self, id_corporate_client, id_contract, id_wallet_type, id_struct, name_file_origin):
        return self.repository.find_register(id_corporate_client, id_contract, id_wallet_type, id_struct,
                                             name_file_origin)

    def save(self, input_log: InputLogModel):
        if input_log is None:
            raise ExceptionData(0, "input_log is necessary")

        entity = LogEntradaEntity(
            id_seqlog=input_log.id_seqlog,
            id_clientecorporativo=input_log.id_clientecorporativo,
            id_contrato=input_log.id_contrato,
            data=input_log.data,
            id_tipocarteira=input_log.id_tipocarteira,
            hora=input_log.hora,
            arquivoorigem=input_log.arquivoorigem,
            totalregistros=input_log.totalregistros,
            regcorretos=input_log.regcorretos,
            regincorretos=input_log.regincorretos,
            regenvinclusao=input_log.regenvinclusao,
            regenvalteracao=input_log.regenvalteracao,
            regenvcancelamento=input_log.regenvcancelamento,
            regenvreativacao=input_log.regenvreativacao,
            regenvprecadastro=input_log.regenvprecadastro,
            regenverro=input_log.regenverro,
            regenvfatal1=input_log.regenvfatal1,
            regenvfatal2=input_log.regenvfatal2,
            regenvvencido=input_log.regenvvencido,
            regenvlote=input_log.regenvlote,
            data_fim=input_log.data_fim,
            hora_fim=input_log.hora_fim,
            status=input_log.status,
            id_estrutura=input_log.id_estrutura,
            regjacadastrado=input_log.regjacadastrado,
            regnaoprocessado=input_log.regnaoprocessado,
            regignorado=input_log.regignorado,
            regduplicadocarga=input_log.regduplicadocarga
        )
        return self.repository.insert(entity)
