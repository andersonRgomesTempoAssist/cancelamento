from kink import inject

from data.helpers import ExceptionData
from infra.helpers import ExceptionRepository
from infra.repositories.contract_repository import ContractRepository


@inject()
class ContractDataUseCase:

    def __init__(self, repository: ContractRepository):
        self.repository = repository

    def find_by_id(self, id_contract):

        if id_contract is None:
            raise ExceptionData(0, 'id_contract is necessary')
        try:
            return self.repository.find_by_id(id_contract)

        except ExceptionRepository as error:
            raise ExceptionData(error.code, error.message)
        except Exception as error:
            print(error)
            raise ExceptionData('000', 'Internal Server Error')
