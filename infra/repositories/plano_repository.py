from collections import namedtuple

import sqlalchemy
from sqlalchemy.orm.exc import NoResultFound

from infra.config import DBConnectionHandler
from infra.entities import PlanoEntity
from infra.helpers import ExceptionRepository


class PlanoRepository:
    """Class Plano Repository"""

    def find_by_id(self, id_plano):
        for i in DBConnectionHandler.__session__():
            try:
                return i.query(PlanoEntity).filter(PlanoEntity.id == id_plano).one()
            except sqlalchemy.exc.OperationalError as error:
                raise ExceptionRepository('000', 'Internal server Error')
                i.throw(error)
            except NoResultFound:
                raise ExceptionRepository('000', 'Not found Plans')
            except Exception as error:
                raise ExceptionRepository('000', 'Internal server Error')
                i.throw(error)

    def find_by_id_contract(self, id_contract):
        for i in DBConnectionHandler.__session__():
            try:
                Plano = namedtuple(
                    "Plano",
                    [
                        "id_plano",
                        "id_contrato",
                        "id_status",
                        "iniciovigencia",
                        "fimvigencia",
                        "datacadastro"
                    ])
                results = i.query(PlanoEntity).filter(PlanoEntity.id_contrato == id_contract).all()
                if results is None or len(results) == 0:
                    raise NoResultFound
                return [Plano(id_plano=result.id, id_contrato=result.id_contrato,
                              id_status=result.id_status, iniciovigencia=result.iniciovigencia,
                              fimvigencia=result.fimvigencia, datacadastro=result.datacadastro) for result in
                        results]
            except sqlalchemy.exc.OperationalError as error:
                raise ExceptionRepository('000', 'Internal server Error')
                i.throw(error)
            except NoResultFound:
                raise ExceptionRepository('000', 'Not found Plans')
            except Exception as error:
                raise ExceptionRepository('000', 'Internal server Error')
                i.throw(error)
