from collections import namedtuple

from kink import inject

from data.helpers import ExceptionData
from infra.helpers import ExceptionRepository
from infra.repositories import PlanoRepository


@inject()
class PlanoDataUseCase:

    def __init__(self, repository: PlanoRepository):
        self.repository = repository

    def find_by_id(self, id_plan):

        if id_plan is None:
            raise ExceptionData(0, 'id_plan is necessary')
        try:
            return self.repository.find_by_id(id_plan)
            Plano = namedtuple(
                "Plano",
                [
                    "id_plano",
                    "id_contrato",
                    "id_status",
                    "iniciovigencia",
                    "fimvigencia",
                    "datacadastro"
                ])
            return Plano(id_plano=plano_entity.id, id_contrato=plano_entity.id_contrato,
                         id_status=plano_entity.id_contrato, iniciovigencia=plano_entity.iniciovigencia,
                         fimvigencia=plano_entity.fimvigencia, datacadastro=plano_entity.datacadastro)
        except ExceptionRepository as error:
            raise ExceptionData(error.code, error.message)
        except Exception as error:
            raise ExceptionData('000', 'Internal Server Error')

    def find_by_id_contract(self, id_contract):
        if id_contract is None:
            raise ExceptionData(0, 'id_contract is necessary')
        try:
            return self.repository.find_by_id_contract(str(id_contract))
        except ExceptionRepository as error:
            raise ExceptionData(error.code, error.message)
        except Exception as error:
            raise ExceptionData('000', 'Internal Server Error')
