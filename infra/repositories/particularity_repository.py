from collections import namedtuple

import sqlalchemy

from infra.config import DBConnectionHandler
from infra.entities import ParticularityEntity
from infra.helpers import ExceptionRepository


class ParticularityRepository:

    def find_list_id_covered_item(self, covered_item: []):
        for i in DBConnectionHandler.__session__():
            try:
                Record = namedtuple("Record", [
                    "id_particularidade",
                    "id_itemcoberto",
                    "id_tipoparticularidade",
                    "valor"
                ])
                results = None
                if len(covered_item) > 1000:
                    filter_select = ""
                    str_query = "SELECT id_particularidade, id_itemcoberto, id_tipoparticularidade, VALOR " \
                                "FROM PARTICULARIDADE WHERE 1=1 "

                    n = len(covered_item) // 1000 + 1
                    splinted = [covered_item[i::n] for i in range(n)]
                    for sp in splinted:
                        filter_select += " OR ID_ITEMCOBERTO IN (" + str(sp).replace('[', '').replace(']', '') + ")"
                    str_query += " AND (" + filter_select[3:] + ")"
                    results = i.execute(str_query)
                    Record = namedtuple("Record", results.keys())
                    records = [Record(*r) for r in results.fetchall()]
                    return records
                else:
                    results = i.query(ParticularityEntity).filter(
                        ParticularityEntity.id_itemcoberto.in_(covered_item)).all()
                    return [
                        Record(
                            id_particularidade=result.id_particularidade,
                            id_itemcoberto=result.id_itemcoberto,
                            id_tipoparticularidade=result.id_tipoparticularidade,
                            valor=result.valor,
                        )
                        for result in results
                    ]
            except sqlalchemy.exc.OperationalError as error:
                raise ExceptionRepository('000', error)
            except Exception as error:
                raise ExceptionRepository('000', error)

    def insert(self, entities):
        for i in DBConnectionHandler.__session__():
            try:
                i.bulk_save_objects(entities, return_defaults=True)
                i.commit()
                return entities
            except sqlalchemy.exc.OperationalError as error:
                raise ExceptionRepository('000', error)
            except Exception as error:
                raise ExceptionRepository('000', error)

    def update(self, entities):
        mappings = []
        mappings_insert = []

        for entity in entities:
            if entity is not None and entity.id_particularidade is not None:
                mapper = {'id_particularidade': entity.id_particularidade,
                          'id_itemcoberto': entity.id_itemcoberto,
                          'id_tipoparticularidade': entity.id_tipoparticularidade,
                          'valor': entity.valor
                          }
                mappings.append(mapper)
            elif entity is not None:
                mappings_insert.append(entity)

        if len(mappings_insert) > 0:
            self.insert(mappings_insert)

        for i in DBConnectionHandler.__session__():
            try:
                i.bulk_update_mappings(ParticularityEntity, mappings)
                i.commit()
                return entities
            except sqlalchemy.exc.OperationalError as error:
                raise ExceptionRepository('000', error)
            except Exception as error:
                raise ExceptionRepository('000', error)
